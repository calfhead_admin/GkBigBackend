from http.client import HTTPException
from flask import Flask, render_template, jsonify
from flask_cors import CORS
from settings import DevConfig,here
from .models import db,User
from CreateUser.controllers import auth
from CreateUser.extensions import cors,lm,bcrypt,oid
from uploads import up
from SQL_All import sql
from SQL_All import logonav
import os


def create_app():

    app = Flask(
        __name__,
        static_folder=os.path.join(here,'dist'),
        static_url_path='/'
        # template_folder=FRONTEND_FOLD
        # ER
    )
    print("静态文件夹路径:",os.path.join(here,'dist'))
    
    app.config['JSON_AS_ASCII'] = False
    #app.config['JSONIFY_MIMETYPE'] = "application/json;charset=utf-8"
    app.config.from_object(DevConfig)
    #app的额外扩展
    db.init_app(app)
    #csrfp.init_app(app)
    CORS(app, resources=r'/*')
    bcrypt.init_app(app)
    oid.init_app(app)
    lm.init_app(app)

    @lm.user_loader
    def load_user(uid):
        return User.query.get(uid)

    # #测试
    # @app.route('/')
    # def index():
    #     return render_template('index.html')

    #测试
    @app.errorhandler(404)
    def page_not_found(error):
        return render_template('index.html'),404

    #注册登录注册的蓝图
    app.register_blueprint(auth.bp)
    #注册上传的蓝图
    app.register_blueprint(up.folder)
    # 学院蓝图
    app.register_blueprint(logonav.bp_nav)
    #注册sql的蓝图
    app.register_blueprint(sql.select)


    return app


def register_error_handlers(app):
    @app.errorhandler(HTTPException)
    def handle_http_error(exc):
        return jsonify({'status': 'error', 'description': exc.description}), exc.code
