import os

from CreateUser import create_app
from flask_script import Manager,Server
from flask_migrate import MigrateCommand,Migrate
from CreateUser.models import db,User
from uploads.up  import get_upload_data
app=create_app()

migrate=Migrate(app,db)

manager=Manager(app=app)
manager.add_command("server",Server(host='0.0.0.0'))
manager.add_command("db",MigrateCommand)

# 修复数据库，将解析了的文件，数据库里多余的都删除
# @manager.command
# def fix():
#     pass
#     uploaded = get_upload_data()
#     print("获取上传的文件..")
#     for item in uploaded:
#         print(item)
    
# # 扫描上传的csv文件






@manager.shell
def make_shell_context():
    return dict(app=app,db=db,User=User)


if __name__ == '__main__':
    manager.run()
    # vs code 调试时使用
    # app.run(host='0.0.0.0')


