# GkBigBackend

#### 介绍
GK学生综合素质项目，将GK学生的综合素质数字形式变成可视化的形式，更直观的展示学生各个方面的实力。

#### 软件架构
软件架构说明


#### 安装教程

1. 本地部署安装MySQL，使用账号密码都为 root
2. 安装开发时所需要的包 pip install -r requirements.txt （建议在虚拟环境中）

#### 使用说明

##### 一、迁移数据库
1. 使用命令 python manager.py db init 
2. 使用命令 python manager.py db migrate
3. 使用命令 python manager.py db upgrade
##### 二、开启5000端口服务
1. 使用命令 python manager.py runserver


#### 参与贡献

1. 
2. 
3. 
4. 


#### 码云特技

